import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    animationName: '',
    cards: [
    {
      img: 'coffee',
      text: 'Some text about tea? Tea can be drinked. Also you can pick some cups below!',
      counting: 0,
      price: 2,
      summaryCounted: null
    },
    {
      img: 'tea',
      text: 'Some text about coffee? Coffee is drinkable too :) Feel free to pick some cups below!',
      counting: 0,
      price: 2.5,
      summaryCounted: null
    },
    {
      img: 'water',
      text: 'Some text about water? Water is needed to be drinked :) Pick that up below!',
      counting: 0,
      price: 1.5,
      summaryCounted: null
    },
    {
      img: 'apples',
      text: 'Some text about apples? Apples is tasty and healthy! Take it to your basket!',
      counting: 0,
      price: 4,
      summaryCounted: null
    },
    ]
  },
  getters: {
    inBasket(state) {
      return state.cards.filter(item => item.summaryCounted / item.price !== 0)
    }
  },
  mutations: {
    increment(state, index) {
      state.cards[index].counting++
    },
    decrement(state, index) {
      if(state.cards[index].counting > 0) {
        state.cards[index].counting--
      }
    },
    summary(state, object) {
      state.cards[object.index].summaryCounted += object.summary;
      state.cards[object.index].counting = 0;
    },
    purchase(state) {
      state.cards.forEach(item => item.summaryCounted = 0);
    },
    closing(state, index) {
      state.cards[index].summaryCounted = 0
    },
    animationDependsOnPath(state, name) {
      state.animationName = name
    }
  },
  actions: {
  },
  modules: {
  }
})
